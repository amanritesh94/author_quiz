import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter, Route, withRouter } from 'react-router-dom';
import './index.css';
import App from './App';
import AddAuthorForm from './AddAuthorForm';
import * as serviceWorker from './serviceWorker';
import { shuffle, sample } from 'underscore';

const authors = [
    {
        name: 'A.P.J. Abdul Kalam',
        imageUrl: 'images/authors/apjabdul.jpg',
        imageSource: 'wikimedia',
        books: [
            'Wings of Fire',
            'The 3 Mistakes of My Life ',
            'My Struggle ',
            'Last Days of Netaji '
        ]
    },
    {
        name: 'Amrita Pritam ',
        imageUrl: 'images/authors/AmritaPritam .jpg',
        imageSource: 'wikimedia',
        books: [
            'Forty Nine Days',
            'Eternal India ',
            'Glimpses of World History',
            'India First  '
        ]
    },
    {
        name: 'Anil Padmanaban ',
        imageUrl: 'images/authors/anil.jpeg',
        imageSource: 'wikimedia',
        books: [
            'Kalpana Chawla – A Life',
            'kumarasambhava ',
            'Swapnavasavadatta',
            'Malavikagnimitra'
        ]
    },
    {
        name: 'Anita Desai ',
        imageUrl: 'images/authors/anita-desai-2.png',
        imageSource: 'wikimedia',
        books: [
            'Feasting',
            'Animal Farm (Mass Market Paperback) ',
            'Imagining India ',
            'Ayodhya'
        ]
    },
    {
        name: 'Arundhati Roy',
        imageUrl: 'images/authors/ArundhatiRoy.jpg',
        imageSource: 'wikimedia',
        books: [
            'The God of Small Things',
            'Arthashastra ',
            'Women and Men in My Life',
            'Train To Pakistan '
        ]
    },
    {
        name: 'Amitav Ghosh',
        imageUrl: 'images/authors/AmitavGhosh.jpg',
        imageSource: 'wikimedia',
        books: [
            'Shadow Lines',
            'My Nation My Life ',
            'Unhappy India ',
            'My Experiments with Truth '
        ]
    }
];
function getTurnData(authors) {
    const allBooks = authors.reduce(function (p, c, i) {
        return p.concat(c.books);
    }, []);

    const fourRandomBooks = shuffle(allBooks).slice(0, 4);
    const answer = sample(fourRandomBooks);
    return {
        books: fourRandomBooks,
        author: authors.find((author) =>
            author.books.some((title) =>
                title === answer))
    }
}
function resetState() {
    return {
        turnData: getTurnData(authors),
        highlight: ''
    }
}
let  state = resetState();

function onAnswerSelected(answer) {
    const isCorrect = state.turnData.author.books.some((book) => book === answer);
    state.highlight = isCorrect ? 'correct' : 'wrong';
    render();

}

function AppRouter() {
    return <App {...state} onAnswerSelected={onAnswerSelected}
    onContinue={ () => {
        state= resetState();
        render();
    }} />;
}

const AuthorWrapper = withRouter(({ history}) => 
     <AddAuthorForm onAddAuthor={(author) => {
        authors.push(author);
        history.push('/');
        }} />
);

function render() {
    ReactDOM.render(
        <BrowserRouter>
            <React.Fragment>
                <Route exact path='/' component={AppRouter} />
                <Route path='/add' component={AuthorWrapper} />
            </React.Fragment>
        </BrowserRouter>, document.getElementById('root'));
}


render();
serviceWorker.unregister();
